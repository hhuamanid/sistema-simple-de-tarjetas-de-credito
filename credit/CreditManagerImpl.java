package credit;

import java.rmi.*;
import java.rmi.server.*;
import java.util.HashMap;

public class CreditManagerImpl extends UnicastRemoteObject implements CreditManager {
  private static transient HashMap accounts = new HashMap();

  /**
   * This is the default class constructor that does nothing
   * but implicitly call super().
   */
  public CreditManagerImpl() throws RemoteException {
  }

  /**
   * Creates a new account. Puts the customer name and the customer's
   * credit card in the hashtable.
   */
  public CreditCard newCreditAccount(String customerName) throws RemoteException {
    CreditCardImpl newCard = null;
    try {
      newCard = new CreditCardImpl(customerName);
    } catch (DuplicateAccountException e) {
      return null;
    }
    accounts.put(customerName, newCard);
    return newCard;
  }

  /**
   * Searches the hashtable for an existing account. If no account
   * for customer name, one is created and added to hashtable.
   * Returns the account.
   */
  public CreditCard findCreditAccount(String customer) throws DuplicateAccountException, RemoteException {
    CreditCardImpl account = (CreditCardImpl) accounts.get(customer);
    if (account != null) {
      return account;
    }
    // Create new account. Add credit card to hashtable.
    account = new CreditCardImpl(customer);
    accounts.put(customer, account);
    return account;
  }
}