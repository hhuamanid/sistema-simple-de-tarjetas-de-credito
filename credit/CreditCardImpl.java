package credit;

import java.rmi.*;
import java.rmi.server.*;
import java.io.Serializable;

/**
 * This class is the remote object that will referenced by the skeleton
 * on the server side and the stub on the client side.
 */
public class CreditCardImpl extends UnicastRemoteObject implements CreditCard, Serializable {
  private float currentBalance = 0;
  private float creditLine = 5000f;
  private int signature = 0; // Like a p.i.n. number
  private String accountName; // Name of owner

  /** Class constructor generates an initial pin. */
  public CreditCardImpl(String customer) throws RemoteException, DuplicateAccountException {
    accountName = customer;
    signature = (int) (Math.random() * 10000);
  }

  /** Returns credit line. */
  public float getCreditLine() throws RemoteException {
    return creditLine;
  }

  /** Pays off some debt. */
  public void payTowardsBalance(float money) throws RemoteException, InvalidMoneyException {
    if (money <= 0) {
      throw new InvalidMoneyException();
    } else {
      currentBalance -= money;
    }
  }

  /** Changes signature. */
  public void setSignature(int pin) throws RemoteException {
    signature = pin;
  }

  /**
   * Makes a purchase. Makes sure enough credit is available,
   * then increments balance and decrements available credit.
   */
  public void makePurchase(float amount, int signature) throws RemoteException, InvalidSignatureException,
      CreditLineExceededException {
    if (signature != this.signature) {
      throw new InvalidSignatureException();
    }
    if (currentBalance + amount > creditLine) {
      throw new CreditLineExceededException();
    } else {
      currentBalance += amount;
      creditLine -= amount;
    }
  }
}
