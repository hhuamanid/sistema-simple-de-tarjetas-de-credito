package credit;

import java.rmi.*;

public interface CreditCard extends Remote {
  /** This method returns a credit card's credit line. */
  public float getCreditLine() throws RemoteException;

  /**
   * This method allows a card holder to pay all or some
   * of a balance. Throws InvalidMoneyException if the
   * money param is invalid.
   */
  public void payTowardsBalance(float money) throws RemoteException, InvalidMoneyException;

  /**
   * This method allows the cardholder to make purchases
   * against the line of credit. Throws
   * CreditLineExceededException
   * if the purchase exceeds available credit.
   */
  public void makePurchase(float amount, int signature) throws RemoteException, InvalidSignatureException, CreditLineExceededException;

  /** This method sest the card's personal i.d. signature. */
  public void setSignature(int pin) throws RemoteException;
}