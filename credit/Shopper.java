package credit;

import java.rmi.*;

public class Shopper {
  public static void main(String args[]) {
    CreditManager cm = null;
    CreditCard account = null;
    // Check the command line.
    if (args.length != 3) {
      System.err.println("Usage:");
      System.err.println("java credit.Shopper <server> <port> <account name>");
      System.exit(1);
    }
    String server = args[0];
    int port = Integer.parseInt(args[1]);
    String name = args[2];
    // Create and install a security manager.
    // System.setSecurityManager(new RMISecurityManager());
    // Obtain reference to card manager.
    try {
      String url = new String("//" + server + ":" + port + "/cardManager");
      System.out.println("Shopper: lookup cardManager, url = " + url);
      cm = (CreditManager) Naming.lookup(url);
    } catch (Exception e) {
      System.err.println("Error in getting card manager" + e);
    }
    // Get user's account.
    try {
      account = cm.findCreditAccount(name);
      System.out.println("Found account for " + name);
    } catch (Exception e) {
      System.err.println("Error in getting account for " + name);
    }
    // Do some transactions.
    try {
      System.out.println("Available credit is: " + account.getCreditLine());
      System.out.println("Changing pin number for account");
      account.setSignature(1234);
      System.out.println("Buying a new watch for $100");
      account.makePurchase(100.00f, 1234);
      System.out.println("Available credit is now: " + account.getCreditLine());
      System.out.println("Buying a new pair of shoes for $160");
      account.makePurchase(160.00f, 1234);
      System.out.println("CardHolder: Paying off $136 of balance");
      account.payTowardsBalance(136.00f);
      System.out.println("Available credit is now: " + account.getCreditLine());
    } catch (Exception e) {
      System.err.println("Transaction error for " + name);
    }
  }
}