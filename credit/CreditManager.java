package credit;

import java.rmi.*;

public interface CreditManager extends Remote {
  /**
   * This method finds an existing credit card for a given customer
   * name. If the customer does not have an account, a new card will
   * be "issued" with a random personal i.d. signature and a $5000
   * starting credit line.
   */
  public CreditCard findCreditAccount(String Customer) throws DuplicateAccountException, RemoteException;

  /**
   * This method creates a new Cedit Account with a random
   * personal i.d. signature and a $5000 starting credit line.
   */
  public CreditCard newCreditAccount(String newCustomer) throws RemoteException;
}
