package credit;

import java.rmi.*;
import java.rmi.registry.LocateRegistry;

public class CardBank {
  public static void main(String args[]) {
    if (args.length != 2) {
      System.err.println("Usage:");
      System.err.println("java credit.CardBank <server> <port>");
      System.exit(1);
    }
    String server = args[0];
    int port = Integer.parseInt(args[1]);
    // Create and install a security manager.
    // System.setSecurityManager(new RMISecurityManager());
    try {
      // Specify registry location
      LocateRegistry.createRegistry(port);
      System.out.println("CardBank: defined registry port");
      // Create an instance of our Credit Manager.
      System.out.println("CreditManagerImpl: create a CreditManager");
      CreditManagerImpl cmi = new CreditManagerImpl();
      // Bind the object instance to the remote registry. Use the
      // static rebind() method to avoid conflicts.
      System.out.println("CreditManagerImpl: bind it to a name");
      String urlString = "//" + server + ":" + port + "/" + "cardManager";
      Naming.rebind(urlString, cmi);
      System.out.println("CreditManager is now ready");
    } catch (Exception e) {
      System.out.println("An error occured");
      e.printStackTrace();
      System.out.println(e.getMessage());
    }
  }
}
